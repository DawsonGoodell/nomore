# README #

NoMoRe is a Node+Mongo+React/Redux framework.
Made to be used with [NoMoRe-cli](https://bitbucket.org/DawsonGoodell/nomore-cli).

### Getting started ###

* Install [NVM](https://github.com/creationix/nvm)
* Run `nvm install`
* Run `npm install`
* Run `npm start`

### Contribution guidelines ###

* Add the ESLint plugin to your editor