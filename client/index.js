import 'babel-polyfill';
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { Router, browserHistory } from 'react-router';

import routes from '../app/routes';
import { configureStore } from '../app/redux/store/configureStore';

const initialState = window.__INITIAL_STATE__;
const store = configureStore(initialState);
const dest = document.getElementById('root');

render(
  <Provider store={store}>
    <Router history={browserHistory} routes={routes} />
  </Provider>,
  dest
);
