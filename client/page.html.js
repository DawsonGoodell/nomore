export function renderFullPage(html, initialState) {
  // const cssPath = process.env.NODE_ENV === 'production' ? '/css/app.min.css' : '/css/app.css';
  // const head = Helmet.rewind();

  console.log(html);
  console.log(initialState);

  return `
    <!DOCTYPE html>
    <html>
      <head>
          <meta charset="UTF-8">
          <meta name="HandheldFriendly" content="True">
          <meta name="MobileOptimized" content="320">
          <meta name="viewport" content="width=device-width">
          <title>NoMoRe</title>
          <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700" rel="stylesheet" type="text/css">
      </head>
      <body>
          <div id="root">${html}</div>
          <script>
            window.__INITIAL_STATE__ = ${JSON.stringify(initialState)};
          </script>
          <script src="/application.min.js"></script>
      </body>
    </html>
  `;
}
