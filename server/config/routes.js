import routes from '../routes';

/**
 * Takes the app object and adds routes to it.  Also receives a config object that can be used
 * to define the environment or other critical information.
 *
 * @param {Object} app - The Express app object.
 * @param {Object} [config] - An object holding user configured values.
 */
export default function routesConfig(app) {
  // All server routes go through /api by default.  Attach individual controller
  // routers in /routes/index.js to add to the /api route.
  app.use('/api', routes);

  // Any custom routes handled outside of React can be placed below.
  // app.use('/custom/page', importedRoutes);
}
