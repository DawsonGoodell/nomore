import express from 'express';
import todo from './todo.routes';

const router = new express.Router();

router.use('/todo', todo);

export default router;
