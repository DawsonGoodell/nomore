import { Router } from 'express';
import TodoController from '../controllers/todo.controller';

const router = new Router();

router.route('/getTodos').get(TodoController.getTodos);

export default router;
